# SCEE Info Theme 

This is the standard theme for Jekyll websites of the SCCE Group and related projects.

## Installation

Add this line to your Jekyll site's ```Gemfile```:

```ruby
gem "theme.scce.info"
```

And add this line to your Jekyll site's ```_config.yml```:

```yaml
theme: theme.scce.info
```

And then execute:

```bash
$ bundle
```

## Usage

The SCCE Theme is customized in two ways: 

- Its general appearance can be customized though the ```_config.yml```. This allows to customize the theme's branding, its meta data, and repeating content. 
- Both header and footer navigation are customized through the pages' front matter. This way pages register themselves in the navigation. 

### Global Theme Configuration

The general appearance can be customized though the ```_config.yml```. In particular, the following entries are considered for branding, meta data, and repeating content: 

- ```theme``` must be set to ```theme.scce.info``` to enable this theme. 
- ```title``` is included in the website's meta data, 
- ```description``` is included in the website's meta data and displayed in right half of the footer,
- ```author``` is included in the website's meta data and displayed in the footer, 
- ```email``` is displayed in the footer below the author, 
- ```url``` is used to render absolute links, 
- ```fav-icon``` defines the paths to favourite icons of different sizes. The definition must contain ```*``` to be replaced with the pixel size of the icon (16, 32, 64, 128, 256). For example, if ```fav-icon``` is set to ```/assets/fav-icons/fav-*.png``` the files ```fav-16.png```, ```fav-32.png```, ..., ```fav-256.png``` must be present in the directory ```/assets/fav-icons/```. 
- ```header.logo.image``` defines the header image or logo that is displayed in the header's center. 
- ```header.logo.url``` (optional) defines the header image's link. This must be an absolute link. If the attribute is not set the header image will link to the website's home page.  
- ```footer.logo.image``` defines the footer's image or logo that is displayed at its left. 
- ```footer.logo.url``` (optional) defines the footer image's link. This must be an absolute link. If the attribute is not set the header image will link to the website's home page. 

Here is an example of what the ```_config.yml``` may look like:

```
title: SCCE - Service Centered Continuous Engineering
author: SCCE Group
email: info@scce.info
url: https://www.scce.info
description: Service Centered Continuous Engineering (SCCE) aims at...
theme: theme.scce.info

fav-icon: /assets/fav-icons/scce-fav-*.png
header:
  logo: 
    image: /assets/logos/scce-logo-center.svg
footer:
  logo: 
    image: /assets/logos/scce-logo-left.svg
    url: https://www.scce.info
```

### Favourite Icon 

The path to the favourite icon is defined gloibally with the ```fav-icon``` attribute where - as explained above - various icon sized (16, 32, 64, 128, 256) are expected. A neat way to realize this would be a single svg icon from which different icon sizes can be generated with a command like the following

```
convert -background none -resize 64x64 fav.svg fav-64.png   
```

### Header and Footer Navigation 

Both header and footer navigation are customized through the pages' front matter. This way pages register themselves in one of the navigations if desired. 

When building the navigations, the following attributes are considered: 

- ```navigation.title``` (optional) labels the menu item if set. Otherwise the page's title ```title``` will be used instead. 
- ```navigation.where``` chooses one of the two navigations ````header``` or ```footer```. 
- ```navigation.category``` (optional) causes the menu item to appear in a dropdown menu with this label. 
- ```navigation.order``` (optional) allows to customize the order of menu items. This can be either the order within a dropdown menu or the order in the top-level navigation depending on whether or not ```navigation.category``` was set. 

Here is an example of what a page's front matter may look like:

```
---
layout: page
title: Service Centered Continuous Engineering
permalink: /
navigation: 
  title: Home
  where: header
  order: 1
---

# Service Centered Continuous Engineering

...
```

## Development

To set up your environment to develop this theme, run ```bundle install```.

Your theme is setup just like a normal Jekyll site! To test your theme, run ```jekyll serve``` and open your browser at ```http://localhost:4000```. This starts a Jekyll server using your theme. Add pages, documents, data, etc. like normal to test your theme's contents. As you make modifications to your theme and to your content, your site will regenerate and you should see the changes in the browser after a refresh, just like normal.

When your theme is released, only the files in ```_layouts```, ```_includes```, ```_sass``` and ```assets``` tracked with Git will be bundled. To add a custom directory to your theme-gem, please edit the regexp in ```theme.scce.info.gemspec``` accordingly.

## License

The theme is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

